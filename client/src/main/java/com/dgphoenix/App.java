package com.dgphoenix;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class App
{
    public static void main( String[] args ) {
        try {
            GameClient gameClient = new GameClient(Files.readAllLines(Paths.get(args[0])));

            try {
                gameClient.start();
            } catch (GameClient.GameClientException e) {
                System.out.println(e.getMessage());
            }
        } catch (IOException e) {
            System.out.println("Error while opening file: " + args[0]);
        }
    }
}
