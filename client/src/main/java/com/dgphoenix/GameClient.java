package com.dgphoenix;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class GameClient {
    class GameClientException extends Exception {
        public GameClientException(String message) {
            super(message);
        }
    }

    private static final int CHECK_IF_STARTED_INTERVAL = 1000;

    private int requestIntervalMSecs;
    private int currentIndex;
    private int dictionarySize;
    private String url = "http://localhost:8080/server/";

    private List<String> words;
    private Set<String> dictionary;

    public GameClient(List<String> words)  {
        this.words = words;
        this.dictionary = new TreeSet<>();
    }

    public void start() throws GameClientException {
        connectToGameServer();
        System.out.println("Waiting for game to get started...");
        checkIfStarted();
    }

    private void connectToGameServer() throws GameClientException {
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
                    .asJson();

            JSONObject jsonObject = jsonResponse.getBody().getObject();

            if (!Boolean.parseBoolean(jsonObject.get("accepted").toString())) {
                throw new GameClientException("Client has been rejected by the server");
            }

            requestIntervalMSecs = Integer.parseInt(jsonObject.get("requestIntervalMSecs").toString());
            dictionarySize = Integer.parseInt(jsonObject.get("dictionarySize").toString());

            if (dictionarySize > words.size()) {
                throw new GameClientException("Value of server parameter \"dictionarySize\" is bigger than actual size of client's dictionary");
            }

        } catch (UnirestException e) {
            throw new GameClientException("Error while connecting to server");
        }
    }

    private void checkIfStarted() {
        TimerTask task = new TimerTask() {
            public void run() {
                try {
                    HttpResponse<JsonNode> jsonResponse = Unirest.get(url + "start").asJson();
                    boolean started = Boolean.parseBoolean(jsonResponse.getBody().getObject().get("started").toString());

                    if (started) {
                        play();
                        cancel();
                    }
                } catch (UnirestException e) {
                    e.printStackTrace();
                }
            }
        };

        new Timer().scheduleAtFixedRate(task, 0, CHECK_IF_STARTED_INTERVAL);
    }

    private void play() {
        TimerTask task = new TimerTask() {
            public void run() {
                try {
                    if ((currentIndex == dictionarySize) || (currentIndex == words.size())) {
                        currentIndex = 0;
                    }

                    HttpResponse<JsonNode> jsonResponse = Unirest.get(url + "game")
                            .queryString("word", words.get(currentIndex++))
                            .asJson();

                    JSONObject response = jsonResponse.getBody().getObject();

                    boolean finished = Boolean.parseBoolean(response.get("finished").toString());
                    boolean winner = Boolean.parseBoolean(response.get("winner").toString());

                    if (finished) {
                        System.out.println("Game finished");

                        if (winner) {
                            writeDictionaryToFile();
                        }

                        System.exit(1);
                    }

                    String word = response.get("word").toString();

                    if (!word.isEmpty()) {
                        dictionary.add(word);
                        System.out.println(word);
                    }
                } catch (UnirestException e) {
                    e.printStackTrace();
                }
            }
        };

        new Timer().scheduleAtFixedRate(task, 0, requestIntervalMSecs);
    }

    private void writeDictionaryToFile() {
        try {
            Files.write(Paths.get("result.txt"), dictionary);
        } catch (IOException e) {
            System.out.println("Error while writing to result.txt");
        }
    }
}
