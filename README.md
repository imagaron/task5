# Задача № 5

## Сборка и запуск  

В корневой папке выполнить:

``` 
$ cd client/
$ mvn package
$ cd ../server/
$ mvn package
```

После чего скопировать server.war в папку webapps Tomcat сервера

Запустится сервер, ожидающий 3 клиентов с размером словаря 3 слова (можно изменить в файле game.properties)

Для запуска клиентов выполнить в директории client (каждая команда - в новом терминале):

```
$ java -jar target/client-1.0.jar client1.txt 
$ java -jar target/client-1.0.jar client2.txt
$ java -jar target/client-1.0.jar client3.txt

```

Результат будет записан в файл result.txt
