package com.dgphoenix;

public class GameProvider {

    private static int requiredClientCount;
    private static int dictionarySize;

    public static void setRequiredClientCount(int requiredClientCount) {
        GameProvider.requiredClientCount = requiredClientCount;
    }

    public static void setDictionarySize(int dictionarySize) {
        GameProvider.dictionarySize = dictionarySize;
    }

    private static Game game;

    private GameProvider() {}

    public static void startNewGame() {
        game = new Game(requiredClientCount, dictionarySize);
    }

    public static int getDictionarySize() {
        return game.getDictionarySize();
    }

    public static boolean isClientExist(String id) {
        return game.isClientExist(id);
    }

    public static boolean isThereRoom() {
        return game.isThereRoom();
    }

    public static void addNewClient(String id) {
        game.addNewClient(id);
    }

    public static void addWord(String senderId, String word) {
        game.addWord(senderId, word);
    }

    public static String getWord(String receiverId) {
        return game.getWord(receiverId);
    }

    public static boolean hasFinished() {
        return game.hasFinished();
    }

    public static String getWinnerId() {
        return game.getWinnerId();
    }

    public static int getRequiredClientCount() {
        return game.getRequiredClientCount();
    }
}
