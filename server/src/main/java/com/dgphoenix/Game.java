package com.dgphoenix;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Game {
    private final int requiredClientCount;
    private final int dictionarySize;
    private List<String> clientIdList = new ArrayList<>();
    private Map<String, Set<String>> clientReceivedWords = new ConcurrentHashMap<>();
    private Map<String, String> clientLatestWord = new ConcurrentHashMap<>();
    private Map<String, Boolean> clientFirstWord = new HashMap<>();
    private String winnerId;

    public Game(int requiredClientCount, int dictionarySize) {
        this.requiredClientCount = requiredClientCount;
        this.dictionarySize = dictionarySize;
    }

    public int getDictionarySize() {
        return dictionarySize;
    }

    public boolean isClientExist(String id) {
        return clientIdList.contains(id);
    }

    public boolean isThereRoom() {
        return clientIdList.size() < requiredClientCount;
    }

    public void addNewClient(String id) {
        clientIdList.add(id);
        clientReceivedWords.put(id, new HashSet<>());
        clientFirstWord.put(id, true);
    }

    public void addWord(String senderId, String word) {
        clientLatestWord.put(senderId, word);
    }

    public String getWord(String receiverId) {
        if (clientFirstWord.get(receiverId)) {
            clientFirstWord.put(receiverId, false);
            return "";
        }

        String word = clientLatestWord.get(getPlayerIdExcept(receiverId));

        clientReceivedWords.get(receiverId).add(word);

        if (!hasFinished() && isWinner(receiverId)) {
            setWinnerId(receiverId);
        }

        return word;
    }

    public boolean hasFinished() {
        return winnerId != null;
    }

    public String getWinnerId() {
        return winnerId;
    }

    private void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    private boolean isWinner(String id) {
        return clientReceivedWords.get(id).size() == wordCountToWin();
    }

    private String getPlayerIdExcept(String id) {
        int excludedIndex = clientIdList.indexOf(id);

        do {
            int index = new Random().nextInt(clientIdList.size());
            if (index != excludedIndex) {
                return clientIdList.get(index);
            }
        } while (true);
    }

    private int wordCountToWin() {
        return dictionarySize * (requiredClientCount - 1);
    }

    public int getRequiredClientCount() {
        return requiredClientCount;
    }

    @Override
    public String toString() {
        String info = String.format("Game: %d clients, %d words in dictionary, ", requiredClientCount, dictionarySize);
        info += hasFinished() ? "finished." : "still in progress.";

        return info;
    }
}
