package com.dgphoenix.servlets;

import com.dgphoenix.GameProvider;

import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RootServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getSession().getId();

        if (GameProvider.isThereRoom() && !GameProvider.isClientExist(id)) {
            GameProvider.addNewClient(id);
            writeResponse(resp, getAcceptedResponse());
        } else {
            writeResponse(resp, getRejectedResponse());
        }

        try (PrintWriter out = resp.getWriter()) {
            out.println(req.getSession().getId());
        }
    }

    private String getAcceptedResponse() {
        return Json.createObjectBuilder()
                .add("accepted", true)
                .add("dictionarySize", GameProvider.getDictionarySize())
                .add("requestIntervalMSecs", 1000)
                .build()
                .toString();
    }

    private String getRejectedResponse() {
        return Json.createObjectBuilder()
                .add("accepted", false)
                .build()
                .toString();
    }

    private void writeResponse(HttpServletResponse resp, String json) throws IOException {
        try (PrintWriter out = resp.getWriter()) {
            out.print(json);
        }
    }
}
