package com.dgphoenix.servlets;

import com.dgphoenix.GameProvider;

import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GameServlet extends HttpServlet {

    private int notifiedClientCount;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getSession().getId();

        if (GameProvider.hasFinished()) {
            writeResponse(resp, getFinishedResponse(id));

            if (++notifiedClientCount == GameProvider.getRequiredClientCount()) {
                GameProvider.startNewGame();
                notifiedClientCount = 0;
            }

            return;
        }

        GameProvider.addWord(id, req.getParameter("word"));

        writeResponse(resp, getNextWordResponse(id));
    }

    private String getNextWordResponse(String id) {
        return Json.createObjectBuilder()
                .add("finished", GameProvider.hasFinished())
                .add("winner", id.equals(GameProvider.getWinnerId()))
                .add("word", GameProvider.getWord(id))
                .build()
                .toString();
    }

    private String getFinishedResponse(String id) {
        return Json.createObjectBuilder()
                .add("finished", GameProvider.hasFinished())
                .add("winner", id.equals(GameProvider.getWinnerId()))
                .add("word", "")
                .build()
                .toString();
    }

    private void writeResponse(HttpServletResponse resp, String json) throws IOException {
        try (PrintWriter out = resp.getWriter()) {
            out.print(json);
        }
    }
}
