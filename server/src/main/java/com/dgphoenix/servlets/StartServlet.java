package com.dgphoenix.servlets;

import com.dgphoenix.GameProvider;

import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class StartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (GameProvider.isClientExist(req.getSession().getId()) && !GameProvider.isThereRoom()) {
            writeResponse(resp, getStartedResponse());
        } else {
            writeResponse(resp, getNotStartedResponse());
        }
    }

    private String getStartedResponse() {
        return Json.createObjectBuilder()
                .add("started", true)
                .build()
                .toString();
    }

    private String getNotStartedResponse() {
        return Json.createObjectBuilder()
                .add("started", false)
                .build()
                .toString();
    }

    private void writeResponse(HttpServletResponse resp, String json) throws IOException {
        try (PrintWriter out = resp.getWriter()) {
            out.print(json);
        }
    }
}
