package com.dgphoenix;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@WebListener
public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appConfigPath = rootPath + "game.properties";

        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(appConfigPath));

            GameProvider.setRequiredClientCount(Integer.parseInt(properties.getProperty("requiredClientCount")));
            GameProvider.setDictionarySize(Integer.parseInt(properties.getProperty("dictionarySize")));
        } catch (IOException e) {
            System.out.println("Error while reading properties file");
        }

        GameProvider.startNewGame();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
